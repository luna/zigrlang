const std = @import("std");
const testing = std.testing;

const c = @cImport({
    @cInclude("erl_nif.h");
});

// https://andrealeopardi.com/posts/using-c-from-elixir-with-nifs/

export fn fast_compare(
    env: ?*c.ErlNifEnv,
    argc: c_int,
    argv: [*c]const c.ERL_NIF_TERM,
) c.ERL_NIF_TERM {
    // cool stuff here
    var a: c_int = undefined;
    var b: c_int = undefined;
    var res: c_int = undefined;

    _ = c.enif_get_int(env, argv[0], &a);
    _ = c.enif_get_int(env, argv[1], &b);

    if (a == b) {
        res = 0;
    } else {
        if (a > b) {
            res = 1;
        } else {
            res = -1;
        }
    }

    return c.enif_make_int(env, res);
}

// this can't be saved
// i hand-translated the ERL_NIF_INIT macro manually, still reached some
// bad spots.
var nif_funcs = []c.ErlNifFunc{c.ErlNifFunc{
    .name = c"fast_compare",
    .arity = 2,
    .fptr = fast_compare,
    .flags = 0,
}};

export fn nif_init() *c.ErlNifEntry {
    comptime var entry: c.ErlNifEntry = c.ErlNifEntry{
        .major = 10,
        .minor = 4,
        .name = c"Elixir.FastCompare",
        .num_of_funcs = nif_funcs.len,
        .funcs = @ptrCast([*c]c.ErlNifFunc, &nif_funcs),
        .load = null,
        .reload = null,
        .upgrade = null,
        .unload = null,
        .vm_variant = c"beam.vanilla",
        .options = 1,
        .sizeof_ErlNifResourceTypeInit = @sizeOf(c.ErlNifResourceTypeInit),
        .min_erts = c"erts-10.4",
    };

    return &entry;
}
